QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../src \
               fakeit

HEADERS += fakeit/fakeit.hpp \
../src/entities/CoinDataEntity.h \
../src/interfaces/IAPIHandler.h \
    stubs/APIHandlerStub.h \
../src/models/TrackerModel.h \
../src/models/CoinDataTableModel.h

SOURCES += \
    tst_TrackerModel.cpp \
    ../src/models/TrackerModel.cpp \
    ../src/models/CoinDataTableModel.cpp
