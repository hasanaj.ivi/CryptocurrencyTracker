#include <QtTest>
#include "models/TrackerModel.h"
#include "qtestcase.h"
#include "stubs/APIHandlerStub.h"

class Test : public QObject
{
    Q_OBJECT

public:
    Test();
    ~Test();
    QSharedPointer<APIHandlerStub> handlerStub;
    TrackerModel trackerModel;
private slots:
    void test_refreshCoinDataTableFinished();
    void test_getAveragePricecInDateRange();
    void test_getAveragePricecInDateRangeReversedDates();
    void test_getAveragePricecInDateRangeFail();
};

Test::Test()
    :handlerStub(QSharedPointer<APIHandlerStub>(new APIHandlerStub()))
    ,trackerModel(handlerStub)
{
    handlerStub->queryCoinDataMock = [this](const QString /*&coinCode*/, const QDate /*&date*/)
    {
        const QList<CoinDataEntity> coinDataList
            {
             CoinDataEntity{static_cast<int>(QDateTime::currentDateTime().addDays(-4).toSecsSinceEpoch()),40},
             CoinDataEntity{static_cast<int>(QDateTime::currentDateTime().addDays(-3).toSecsSinceEpoch()),30},
             CoinDataEntity{static_cast<int>(QDateTime::currentDateTime().addDays(-2).toSecsSinceEpoch()),20},
             CoinDataEntity{static_cast<int>(QDateTime::currentDateTime().addDays(-1).toSecsSinceEpoch()),10},
             };
        emit handlerStub->queryFinished(coinDataList);
    };
}

Test::~Test() {}

void Test::test_refreshCoinDataTableFinished()
{
    QSignalSpy spy(&trackerModel, &TrackerModel::refreshFinished);
    trackerModel.refreshCoinDataTable("coincode", 2, QDate::currentDate());

    QCOMPARE(spy.count(), 1);
    QCOMPARE(trackerModel.getTableModel()->data(trackerModel.getTableModel()->index(1,1)), "20");
}

void Test::test_getAveragePricecInDateRange()
{
    trackerModel.refreshCoinDataTable("coincode", 4, QDate::currentDate());
    const auto priceAverage= trackerModel.getAveragePricecInDateRange(QDate::currentDate().addDays(-1),QDate::currentDate().addDays(-4));

    QCOMPARE(priceAverage, (10+20+30+40)/4);
}

void Test::test_getAveragePricecInDateRangeReversedDates()
{
    trackerModel.refreshCoinDataTable("coincode", 4, QDate::currentDate());
    const auto priceAverage= trackerModel.getAveragePricecInDateRange(QDate::currentDate().addDays(-4),QDate::currentDate().addDays(-1));
    const auto priceAverageRev= trackerModel.getAveragePricecInDateRange(QDate::currentDate().addDays(-1),QDate::currentDate().addDays(-4));

    QCOMPARE(priceAverage, priceAverageRev);
}

void Test::test_getAveragePricecInDateRangeFail()
{
    handlerStub->queryCoinDataMock = [this](const QString /*&coinCode*/, const QDate /*&date*/)
    {
        emit handlerStub->queryFinished(QList<CoinDataEntity>());
    };

    trackerModel.refreshCoinDataTable("coincode", 4, QDate::currentDate());

    QVERIFY_THROWS_EXCEPTION(std::exception, trackerModel.getAveragePricecInDateRange(QDate::currentDate().addDays(-40),QDate::currentDate().addDays(-10)));
}

QTEST_APPLESS_MAIN(Test)

#include "tst_TrackerModel.moc"
