#pragma once

#include "interfaces/IAPIHandler.h"
#include <QDate>
class APIHandlerStub
    : public IAPIHandler
{
    Q_OBJECT
public:
    APIHandlerStub()
    {
    }
    std::function<void(const QString &coinCode, const QDate &date)> queryCoinDataMock;

    void queryCoinData(const QString &coinCode, const QDate &date) override
    {
        queryCoinDataMock(coinCode,date);
    }
};
