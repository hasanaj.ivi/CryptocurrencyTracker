#pragma once
#include "entities/CoinDataEntity.h"
#include <QObject>
#include <QString>

class IAPIHandler: public QObject
{
    Q_OBJECT

public:
    virtual void queryCoinData(const QString &coinCode, const QDate &date) = 0;

signals:
    void queryFinished(const QList<CoinDataEntity> &coinDataList);
    void queryError();
};
