#pragma once

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainPresenter : public QMainWindow
{
    Q_OBJECT

public:
    MainPresenter(QWidget *parent = nullptr);
    ~MainPresenter();

private:
    Ui::MainWindow *ui;
};
