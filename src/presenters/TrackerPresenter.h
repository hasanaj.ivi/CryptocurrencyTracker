#pragma once

#include "factories/ModelFactory.h"
#include "models/TrackerModel.h"
#include <QWidget>

namespace Ui {
class TrackerWidget;
}

class TrackerPresenter : public QWidget
{
    Q_OBJECT

public:
    explicit TrackerPresenter(QWidget *parent = nullptr);
    ~TrackerPresenter();
    void initViews();

    void initRefresher();

private slots:
    void calculateAverage();
    void refreshTableView();

private:
    Ui::TrackerWidget *ui;
    QString coinCode = "BTC/USD";
    int lastDaysEntries = 31;
    int tableRefreshTime = 1000*60*60*24;
    int minusDays = -1;
    ModelFactory modelFactory;
    TrackerModel model;
};
