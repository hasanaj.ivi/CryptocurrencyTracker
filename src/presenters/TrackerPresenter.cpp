#include "TrackerPresenter.h"
#include "ui_TrackerWidget.h"
#include <QTimer>
#include <QMessageBox>


TrackerPresenter::TrackerPresenter(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TrackerWidget)
    , model(modelFactory.getModel<TrackerModel>())
{
    ui->setupUi(this);

    connect(ui->calcAvgPushButton, &QPushButton::released, this, &TrackerPresenter::calculateAverage);
    connect(ui->refreshToolButton, &QPushButton::released, this, &TrackerPresenter::refreshTableView);

    initRefresher();
    initViews();

    refreshTableView();
}

void TrackerPresenter::initRefresher()
{
    connect(&model, &TrackerModel::refreshFinished, this, [=](){
        ui->lastUpdateDateLabel->setText(QDateTime::currentDateTime().toString());
    });
    connect(&model, &TrackerModel::refreshError, this, [=](){
        QMessageBox messageBox;
        messageBox.critical(0,"Error",tr("An error has occured while refreshing the table! (check log file)"));
        messageBox.setFixedSize(500,200);
    });

    const auto tableRefreshTimer = new QTimer(this);
    connect(tableRefreshTimer, &QTimer::timeout, this, &TrackerPresenter::refreshTableView);
    tableRefreshTimer->start(tableRefreshTime);
}

void TrackerPresenter::initViews()
{
    ui->tableView->setModel(model.getTableModel());
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->tableInfoLabel->setText(tr("Last %1 days entries:").arg(lastDaysEntries));
    ui->fromDateEdit->setDate(QDate::currentDate().addDays(-lastDaysEntries + minusDays));
    ui->fromDateEdit->setMaximumDate(QDate::currentDate().addDays(minusDays));
    ui->fromDateEdit->setMinimumDate(QDate::currentDate().addDays(-100));

    ui->toDateEdit->setDate(QDate::currentDate().addDays(minusDays));
    ui->toDateEdit->setMaximumDate(QDate::currentDate().addDays(minusDays));
    ui->toDateEdit->setMinimumDate(QDate::currentDate().addDays(-100));
}

void TrackerPresenter::refreshTableView()
{
    model.refreshCoinDataTable(coinCode,lastDaysEntries, QDate::currentDate().addDays(minusDays));
}

void TrackerPresenter::calculateAverage()
{
    try {
        const auto average = QString::number(model.getAveragePricecInDateRange(ui->fromDateEdit->date(),ui->toDateEdit->date()));
        ui->avgLineEdit->setText(average);
    } catch (const std::exception& ex) {
        qWarning() << ex.what();
        ui->avgLineEdit->setText(tr("-error-"));
    }
}

TrackerPresenter::~TrackerPresenter()
{
    delete ui;
}
