#include "MainPresenter.h"
#include "QLayout"
#include "ui_MainWindow.h"
#include "presenters/TrackerPresenter.h"

MainPresenter::MainPresenter(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    const auto trackerWidgetPresenter = new TrackerPresenter(ui->centralwidget);
    ui->centralwidget->layout()->addWidget(trackerWidgetPresenter);
}

MainPresenter::~MainPresenter()
{
    delete ui;
}
