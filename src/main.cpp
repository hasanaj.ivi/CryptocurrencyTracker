#include "adaptors/LoggerAdapter.h"
#include "presenters/MainPresenter.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    LoggerAdapter::initialize();
    MainPresenter mainWindowPresenter;
    mainWindowPresenter.show();
    return app.exec();
}
