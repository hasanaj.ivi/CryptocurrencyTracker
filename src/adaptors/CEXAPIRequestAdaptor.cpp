#include "CEXAPIRequestAdaptor.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkRequest>
#include <QJsonArray>
#include <QTimer>

CEXAPIRequestAdaptor::CEXAPIRequestAdaptor()
    : networkManager()
{
    connect(&networkManager, &QNetworkAccessManager::finished, this, &CEXAPIRequestAdaptor::onQueryFinished);
}

void CEXAPIRequestAdaptor::queryCoinData(const QString &coinCode, const QDate &date)
{
    const auto urlString = QString(cEXAPIUrl).arg(date.toString(apiDateFormat),coinCode);
    networkManager.get(QNetworkRequest(QUrl(urlString)));
}

QList<CoinDataEntity> CEXAPIRequestAdaptor::convertJsonToCoinData(const QString &data)
{
    QList<CoinDataEntity> coinDataList;

    const auto data1dString = QJsonDocument::fromJson(data.toUtf8()).object()["data1d"].toString();
    if(data1dString.isEmpty())
    {
        throw std::runtime_error("data1d not found on data");
    }

    const auto data1dArray = QJsonDocument::fromJson(data1dString.toUtf8()).array();
    if(data1dArray.isEmpty())
    {
        throw std::runtime_error("data1d array is empty");
    }

    for (const auto data1d : data1dArray) {
        coinDataList.append(CoinDataEntity{data1d[0].toInt(),data1d[4].toDouble()});
    }

    return coinDataList;
}

void CEXAPIRequestAdaptor::onQueryFinished(QNetworkReply *reply)
{
    if (reply->error()) {
        qCritical() << reply->errorString();
        emit queryError();
    }
    else
    {
        const auto answer=QString(reply->readAll());
        if(!answer.isEmpty()){
            try {
                const auto coinData = convertJsonToCoinData(answer);
                emit queryFinished(coinData);
                qInfo() << "received CoinData with size: " << coinData.size();
            } catch (const std::exception& ex) {
                qCritical() << ex.what();
                emit queryError();
            }
        }
        else
        {
            qCritical() << "no answer from reply";
            emit queryError();
        }
    }

}
