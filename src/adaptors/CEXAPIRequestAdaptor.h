#pragma once

#include "entities/CoinDataEntity.h"
#include "interfaces/IAPIHandler.h"
#include <QDateTime>
#include <QList>
#include <QNetworkReply>

class CEXAPIRequestAdaptor
    : public IAPIHandler
{
    Q_OBJECT

public:
    CEXAPIRequestAdaptor();
    void queryCoinData(const QString &coinCode, const QDate &date) override;

private slots:
    void onQueryFinished(QNetworkReply *reply);

private:
    QList<CoinDataEntity> convertJsonToCoinData(const QString &data);

private:
    const QString apiDateFormat="yyyyMMdd";
    const QString cEXAPIUrl="https://cex.io/api/ohlcv/hd/%1/%2";
    QNetworkAccessManager networkManager;
};
