#include "LoggerAdapter.h"
#include "QDir"
#include <QFile>
#include <QDateTime>
#include <QApplication>

void LoggerAdapter::logMessageToFile(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    auto message = qFormatLogMessage(type, context, msg);

    QDir dir(QCoreApplication::applicationDirPath());
    if(!dir.exists("logs"))
    {
        dir.mkdir("logs");
    }
    QFile outFile(dir.absoluteFilePath("logs/log.txt"));

    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << message << Qt::endl << Qt::flush;
}

void LoggerAdapter::initialize()
{
    qSetMessagePattern("[%{time dd.MM.yyyy h:mm:ss.zzz}] %{type} %{function} - %{message}");
    qInstallMessageHandler(logMessageToFile);
}
