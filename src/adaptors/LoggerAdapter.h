#pragma once

#include <QtLogging>
#include <QString>

class LoggerAdapter
{
public:
    static void logMessageToFile(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    static void initialize();
};
