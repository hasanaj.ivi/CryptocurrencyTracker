QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
HEADERS += \
    adaptors/CEXAPIRequestAdaptor.h \
    adaptors/LoggerAdapter.h \
    entities/CoinDataEntity.h \
    factories/ModelFactory.h \
    interfaces/IAPIHandler.h \
    models/CoinDataTableModel.h \
    models/TrackerModel.h \
    presenters/MainPresenter.h \
    presenters/TrackerPresenter.h

SOURCES += \
    adaptors/CEXAPIRequestAdaptor.cpp \
    adaptors/LoggerAdapter.cpp \
    factories/ModelFactory.cpp \
    main.cpp \
    models/CoinDataTableModel.cpp \
    models/TrackerModel.cpp \
    presenters/MainPresenter.cpp \
    presenters/TrackerPresenter.cpp

FORMS += \
    views/MainWindow.ui \
    views/TrackerWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
