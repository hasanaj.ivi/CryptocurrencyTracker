#pragma once

class ModelFactory
{
public:
    template<typename T> T getModel();
};
