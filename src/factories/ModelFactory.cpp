#include "ModelFactory.h"
#include "adaptors/CEXAPIRequestAdaptor.h"
#include "models/TrackerModel.h"

template<>
TrackerModel ModelFactory::getModel<TrackerModel>()
{
    return TrackerModel(QSharedPointer<IAPIHandler>(new CEXAPIRequestAdaptor()));
}

