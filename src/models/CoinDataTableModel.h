#pragma once

#include "entities/CoinDataEntity.h"
#include <QAbstractTableModel>

class CoinDataTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit CoinDataTableModel(QObject *parent = nullptr);
    void setCoinData(const QList<CoinDataEntity> coinDataList);
    void setCurrency(const QString &currency);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

private:
    QList<CoinDataEntity> coinDataList;
    QString currency;
};
