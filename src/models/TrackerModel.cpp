#include "TrackerModel.h"
#include <QDateTime>

TrackerModel::TrackerModel(QSharedPointer<IAPIHandler> APIRequest)
    : tableModel(QScopedPointer<CoinDataTableModel>(new CoinDataTableModel()))
    , apiHandler(APIRequest)
{
    connect(apiHandler.get(), &IAPIHandler::queryFinished, this, &TrackerModel::onQueryFinished);
    connect(apiHandler.get(), &IAPIHandler::queryError, this, &TrackerModel::onQueryError);
}

void TrackerModel::onQueryFinished(const QList<CoinDataEntity> &coinDataList)
{
    this->coinDataList = QList<CoinDataEntity>(coinDataList.rbegin(), coinDataList.rend());
    tableModel->setCoinData(this->coinDataList.mid(0, lastDaysEntries));
    emit refreshFinished();
}

void TrackerModel::onQueryError()
{
    emit refreshError();
}

void TrackerModel::refreshCoinDataTable(const QString &coinCode, int lastDaysEntries, const QDate &date)
{
    this->lastDaysEntries = lastDaysEntries;
    tableModel->setCoinData(QList<CoinDataEntity>());
    tableModel->setCurrency(coinCode);
    apiHandler->queryCoinData(coinCode, date);
}

double TrackerModel::getAveragePricecInDateRange(const QDate &from, const QDate &to)
{
    const auto coinDataRange = getCoinDataInDateRange(from,to);
    const auto sumOperation = [](double result, const CoinDataEntity& obj){return result + obj.price;};
    const double average = coinDataRange.size() == 0 ? 0 : std::accumulate(coinDataRange.begin(), coinDataRange.end(), 0, sumOperation)/coinDataRange.size();
    return average;
}

QList<CoinDataEntity> TrackerModel::getCoinDataInDateRange(const QDate &from, const QDate &to)
{
    if(coinDataList.isEmpty())
    {
        throw std::runtime_error("CoinData is empty");
    }
    const auto firstDate = QDateTime::fromSecsSinceEpoch(coinDataList.first().date).date();
    const auto daysToFirstDate = to.daysTo(firstDate);
    const auto daysFromFirstDate = from.daysTo(firstDate);
    const auto len = std::abs(to.daysTo(from)) + 1;
    return coinDataList.mid(std::min(daysToFirstDate,daysFromFirstDate), len);
}

CoinDataTableModel *TrackerModel::getTableModel()
{
    return tableModel.get();
}
