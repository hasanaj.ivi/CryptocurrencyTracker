#include "CoinDataTableModel.h"
#include <QDateTime>

CoinDataTableModel::CoinDataTableModel(QObject *parent)
    : QAbstractTableModel(parent)
    , coinDataList(QList<CoinDataEntity>())
{
}

void CoinDataTableModel::setCoinData(const QList<CoinDataEntity> coinDataList)
{
    beginResetModel();
    this->coinDataList = coinDataList;
    endResetModel();
}

void CoinDataTableModel::setCurrency(const QString &currency)
{
    this->currency = currency;
}

int CoinDataTableModel::rowCount(const QModelIndex & /*parent*/) const
{
    return coinDataList.size();
}

int CoinDataTableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 2;
}

QVariant CoinDataTableModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole){
        const auto coinData=coinDataList[index.row()];
        switch (index.column()) {
        case 0:
            return QDateTime::fromSecsSinceEpoch(coinData.date).date().toString();
        case 1:
            return QString::number(coinData.price);
        }
    }

    return QVariant();
}

QVariant CoinDataTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return tr("Date");
        case 1:
            return tr("Price (%1)").arg(currency);
        }
    }
    return QVariant();
}
