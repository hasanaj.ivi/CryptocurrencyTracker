#pragma once

#include "interfaces/IAPIHandler.h"
#include "models/CoinDataTableModel.h"
#include <QObject>

class TrackerModel
: public QObject
{
    Q_OBJECT

public:
    TrackerModel(QSharedPointer<IAPIHandler> APIRequest);
    CoinDataTableModel* getTableModel();
    double getAveragePricecInDateRange(const QDate &from, const QDate &to);
    void refreshCoinDataTable(const QString &coinCode, int lastDaysEntries, const QDate &date);

signals:
    void refreshFinished();
    void refreshError();

private slots:
    void onQueryFinished(const QList<CoinDataEntity> &coinDataList);
    void onQueryError();

private:
    QList<CoinDataEntity> getCoinDataInDateRange(const QDate &from, const QDate &to);

private:
    QScopedPointer<CoinDataTableModel> tableModel;
    QSharedPointer<IAPIHandler> apiHandler;
    int lastDaysEntries;
    QList<CoinDataEntity> coinDataList;
};
